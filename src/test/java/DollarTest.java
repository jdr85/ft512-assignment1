import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class DollarTest {
    @Test
    @DisplayName("Multiplication")
    public void testMultiplication() {
        Money five = Money.dollar(5);
        assertEquals(Money.dollar(10), five.times(2));
        assertEquals(Money.dollar(15), five.times(3));
    }
    public void testDifferentClassEquality() {
        assertTrue(new Money(10, "CHF").equals(new Franc(10, "CHF")));
    }
    public void testEquality() {
        assertTrue(Money.dollar(5).equals(Money.dollar(5)));
        assertFalse(Money.dollar(5).equals(Money.dollar(6)));
        assertFalse(Money.franc(5).equals(Money.dollar(5)));
    }
    public void testSimpleAddition() {
        Money five= Money.dollar(5);
        Expression sum= five.plus(five);
        Bank bank= new Bank();
        Money reduced= bank.reduce(sum, "USD");
        assertEquals(Money.dollar(10), reduced);
    }
    public void testPlusReturnsSum() {
        Money five= Money.dollar(5);
        Expression result= five.plus(five);
        Sum sum= (Sum) result;
        assertEquals(five, sum.augend);
        assertEquals(five, sum.addend);
    }
    public void testReduceSum() {
        Expression sum= new Sum(Money.dollar(3), Money.dollar(4));
        Bank bank= new Bank();
        Money result= bank.reduce(sum, "USD");
        assertEquals(Money.dollar(7), result);
    }
//    public void testReduceMoneyDifferentCurrency() {
//        Bank bank= new Bank();
//        bank.addRate("CHF", "USD", 2);
//        Money result= bank.reduce(Money.franc(2), "USD");
//        assertEquals(Money.dollar(1), result);
//    }

//    public void testReduceMoneyDifferentCurrency() {
//        Bank bank= new Bank();
////        bank.addRate("CHF", "USD", 2);
//        Money result= bank.reduce(Money.franc(2), "USD");
//        assertEquals(Money.dollar(1), result);
//    }
    public void testArrayEquals() {
        assertEquals(new Object[] {"abc"}, new Object[] {"abc"});
    }
    public void testMixedAddition() {
        Money fiveBucks= Money.dollar(5);
        Money tenFrancs= Money.franc(10);
        Bank bank= new Bank();
//        bank.addRate("CHF", "USD", 2);
        Money result= bank.reduce(fiveBucks.plus(tenFrancs), "USD");
        assertEquals(Money.dollar(10), result);
    }
    public void testPlusSameCurrencyReturnsMoney() {
        Expression sum= Money.dollar(1).plus(Money.dollar(1));
        assertTrue(sum instanceof Money);
    }










    public void testCurrency() {
        assertEquals("USD", Money.dollar(1).currency());
        assertEquals("CHF", Money.franc(1).currency());
    }
//    public void testEquality() {
//        assertTrue(Money.dollar(5).equals(Money.dollar(5)));
//        assertFalse(Money.dollar(5).equals(Money.dollar(6)));
//        assertTrue(new Franc(5).equals(new Franc(5)));
//        assertFalse(new Franc(5).equals(new Franc(6)));
//        assertFalse(new Franc(5).equals(Money.dollar(5)));
//    }





}
