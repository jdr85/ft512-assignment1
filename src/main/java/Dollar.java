public class Dollar extends Money {
    private String currency;
    Dollar(int amount, String currency) {
        super(amount, currency);
    }
    static Money dollar(int amount) {
        return new Money(amount, "USD");
    }

    String currency() {
        return currency;
    }
    private int amount;
    public Money times(int multiplier) {
        return new Money(amount * multiplier, currency);
    }




    public boolean equals(Object object) {
        Money money= (Money) object;
        return amount == money. amount;
    }






}
